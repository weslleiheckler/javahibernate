package Conexao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.Session;

/**
 *
 * @author Wesllei Heckler
 */
public class GenericDao {

    private final Session session;

    public GenericDao() {
        session = HibernateUtil.getSession();
    }

    private Session getSession() {
        return session;
    }

    private void closeSession() {
        if (getSession() != null && getSession().isOpen()) {
            try {
                getSession().close();
            } catch (Exception e) {
                System.out.println("Falha ao fechar a sessão."
                        + "\nMensagem original: " + e.getMessage());
            }
        }
    }

    public void save(Object objeto) {
        try {
            getSession().getTransaction().begin();
            getSession().save(objeto);
            getSession().getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Falha ao salvar o registro no banco de dados."
                    + "\nMensagem original: " + e.getMessage());
            getSession().getTransaction().rollback();
        } finally {
            closeSession();
        }
    }

    public void update(Object objeto) {
        try {
            getSession().getTransaction().begin();
            getSession().update(objeto);
            getSession().getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Falha ao atualizar o registro no banco de dados."
                    + "\nMensagem original: " + e.getMessage());
            getSession().getTransaction().rollback();
        } finally {
            closeSession();
        }
    }

    public void delete(Object objeto) {
        try {
            getSession().getTransaction().begin();
            getSession().delete(objeto);
            getSession().getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Falha ao deletar o registro do banco de dados."
                    + "\nMensagem original: " + e.getMessage());
            getSession().getTransaction().rollback();
        } finally {
            closeSession();
        }
    }

    public List findAll(Class persistentClass) {
        List<Object> lista = new ArrayList<Object>();

        try {
            // Create query
            CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery(persistentClass);
            Root<Object> root = criteriaQuery.from(persistentClass);
            criteriaQuery.select(root);

            // Execute query
            lista = getSession().createQuery(criteriaQuery).getResultList();
        } catch (Exception e) {
            System.out.println("Falha ao consultar os registros no banco de dados."
                    + "\nMensagem original: " + e.getMessage());
        } finally {
            closeSession();
        }

        return lista;
    }

    public Object findById(Class persistentClass, long id) {
        Object object = new Object();

        try {
            // Create query
            CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery(persistentClass);
            Root<Object> root = criteriaQuery.from(persistentClass);
            criteriaQuery.select(root);
            criteriaQuery.where(criteriaBuilder.equal(root.get("id"), id));

            // Execute query
            object = getSession().createQuery(criteriaQuery).getSingleResult();
        } catch (Exception e) {
            System.out.println("Falha ao consultar o registro no banco de dados."
                    + "\nMensagem original: " + e.getMessage());
            object = null;
        } finally {
            closeSession();
        }

        return object;
    }

    public List findByIdAndNome(Class persistentClass, long id, String nome) {
        List<Object> lista = new ArrayList<Object>();

        try {
            // Create query
            CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery(persistentClass);
            Root<Object> root = criteriaQuery.from(persistentClass);
            criteriaQuery.select(root);
            criteriaQuery.where(criteriaBuilder.equal(root.get("id"), id), criteriaBuilder.equal(root.get("nome"), nome));

            // Execute query
            lista = getSession().createQuery(criteriaQuery).getResultList();
        } catch (Exception e) {
            System.out.println("Falha ao consultar os registros por nome no banco de dados."
                    + "\nMensagem original: " + e.getMessage());
        } finally {
            closeSession();
        }

        return lista;
    }

    public List findByIdOrNome(Class persistentClass, long id, String nome) {
        List<Object> lista = new ArrayList<Object>();

        try {
            // Create query
            CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery(persistentClass);
            Root<Object> root = criteriaQuery.from(persistentClass);
            criteriaQuery.select(root);
            Predicate restriction = criteriaBuilder.or(criteriaBuilder.equal(root.get("id"), id), criteriaBuilder.equal(root.get("nome"), nome));
            criteriaQuery.where(criteriaBuilder.or(restriction));

            // Execute query
            lista = getSession().createQuery(criteriaQuery).getResultList();
        } catch (Exception e) {
            System.out.println("Falha ao consultar os registros por nome no banco de dados."
                    + "\nMensagem original: " + e.getMessage());
        } finally {
            closeSession();
        }

        return lista;
    }
}
