package Modelos;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Wesllei Heckler
 */

@Entity
@Table(name = "CONTATO")
public class Contato {
    // Atributos
    @Id
    /*
    * Geração automática de valor:
    *
    * 1 - GenerationType.IDENTITY:
    *   Gera um valor sequencial. Utiliza o recurso de auto incremento do banco de dados para gerar o valor.
    *   @GeneratedValue(strategy = GenerationType.IDENTITY)
    *
    * 2 - GenerationType.SEQUENCE:
    *   Utiliza uma sequence do banco de dados para gerar o valor. Nem todos os bancos de dados possuem esse recurso. 
    *   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")   
    *   @SequenceGenerator(name = "id_generator", sequenceName = "id_seq", allocationSize = 10) => Incrementa 10 no último valor gerado. 
    *
    * 3 - GenerationType.TABLE:
    *   Cria uma tabela no banco de dados para gerenciar a geração do valor automático (simula uma sequence do banco de dados). 
    *   @GeneratedValue(strategy = GenerationType.TABLE, generator = "id_generator")
    *   @TableGenerator(name = "id_generator", table = "id_generator_table", schema = "schema")
    *
    * 4 - GenerationType.AUTO:
    *   Permite que o provedor de persistência escolha a melhor estratégia para gerar o valor automático.
    *   @GeneratedValue(strategy = GenerationType.AUTO)
    *
    */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private long id;
    @Column(name = "NOME", length = 50)
    private String nome;
    @Column(name = "DATA_CADASTRO")
    @Temporal(TemporalType.DATE)
    private Calendar dataCadastro;
    
    // Construtores
    public Contato() {
    }

    public Contato(String nome, Calendar dataCadastro) {
        this.nome = nome;
        this.dataCadastro = dataCadastro;
    }
    
    // Getters and Setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Calendar getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Calendar dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
