package javahibernate;

import Conexao.GenericDao;
import Conexao.HibernateUtil;
import Modelos.Contato;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Wesllei Heckler
 */
public class JavaHibernate {

    public static void main(String[] args) {
        // Insere contato no banco de dados
        GenericDao gd = new GenericDao();
        Contato contato = new Contato("Teste", Calendar.getInstance());
        gd.save(contato);

        // Consulta todos os contatos
        GenericDao gd1 = new GenericDao();
        List<Contato> listaContatos = gd1.findAll(Contato.class);
        Calendar calendar = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setCalendar(calendar);

        System.out.println("Listagem de contatos:");
        for (Contato c : listaContatos) {
            System.out.println(c.getId() + " - " + c.getNome() + " - " + sdf.format(c.getDataCadastro().getTime()));
        }

        // Consulta contato por Id
        GenericDao gd2 = new GenericDao();
        Contato contatoConsultado = (Contato) gd2.findById(Contato.class, 1L);

        System.out.println("Contato consultado:");
        System.out.println(contatoConsultado.getId() + " - " + contatoConsultado.getNome());

        // Atualiza contato
        GenericDao gd3 = new GenericDao();
        contatoConsultado.setNome("Teste alterado");
        gd3.update(contatoConsultado);

        // Exclui contato
        GenericDao gd4 = new GenericDao();
        Contato contatoRemovido = (Contato) gd4.findById(Contato.class, 2L);

        GenericDao gd5 = new GenericDao();
        gd5.delete(contatoRemovido);

        // Consulta contato por Id e Nome
        GenericDao gd6 = new GenericDao();
        List<Contato> listaContatoIdAndNome = gd6.findByIdAndNome(Contato.class, 3L, "Teste");

        System.out.println("Lista de contatos consultados por Id e Nome:");
        for (Contato c : listaContatoIdAndNome) {
            System.out.println(c.getId() + " - " + c.getNome());
        }
        
        // Consulta contato por Id ou Nome
        GenericDao gd7 = new GenericDao();
        List<Contato> listaContatoIdOrNome = gd7.findByIdOrNome(Contato.class, 1L, "Teste");

        System.out.println("Lista de contatos consultados por Id ou Nome:");
        for (Contato c : listaContatoIdOrNome) {
            System.out.println(c.getId() + " - " + c.getNome());
        }

        // Finaliza o HibernateUtil
        HibernateUtil.shutdown();
    }
}
